Multiplayer 21 

* Are there more the 1 player at the table?
    - True: 
        * 
    
* Are there less than 1 card in the deck(max 32)?
	- True:
		* Shuffle deck.
* Is player wallet <= 1?
	- True:
		* Exit Game, player loses
* Bet = 0
* While player bet is 0
	* Ask player how much to bet
	* Is bet > player wallet?
		- True:
			* bet = 0
	* Is bet less than 0?
		- True:
			* Bet = 0
* Take Bet from player wallet
* Deal 1 card to every player
* While player total < 21
	- Ask player "Hit or Stand"
	- If hit:
		* Deal card to player
		* Next loop
	- If stand:
		* exit loop
* Is player total > 21?
	- True:
		* Loss.
		* Next hand.
* While dealer total < 21
	- Ask dealer "Hit or Stand"
	- If hit:
		* Deal card to dealer
		* Next loop
	- If stand:
		* exit loop
* Is player total > dealer total?
	- True:
		* Win
		* Double bet and return to player wallet.
		* Return bet
		* Next Hand
	- False:
		* Loss
		* Next hand.