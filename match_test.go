package main

import (
	"context"
	"database/sql"
	"github.com/gofrs/uuid"
	"github.com/golang/mock/gomock"
	"github.com/heroiclabs/nakama-common/runtime"
	"github.com/heroiclabs/nakama/v2/server"
	"plugin_code/mocks"
	"testing"
)

var Config21 = map[string]interface{}{
	"password": "12345",
	"size":     7,
	"isOpen":   false,
	"title":    "21",
	"minBet":   10.0,
	"maxBet":   100.0,
	"timeout":  5000,
}

func setup(t *testing.T) (nk runtime.NakamaModule, runtimeLogger runtime.Logger, db *sql.DB) {
	db = NewDB(t)
	nk = server.NewRuntimeGoNakamaModule(logger, db,
		nil,
		config,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil,
		nil)
	runtimeLogger = server.NewRuntimeGoLogger(logger)
	return
}

func authenticate(t *testing.T, nk runtime.NakamaModule, db *sql.DB) runtime.Presence {
	userID, _, _, err := server.AuthenticateCustom(context.Background(), logger, db, uuid.Must(uuid.NewV4()).String(), uuid.Must(uuid.NewV4()).String(), true)
	if err != nil {
		t.Fatalf("error creating user: %v", err.Error())
	}
	account, _, err := server.GetAccount(context.Background(), logger, db, nil, uuid.FromStringOrNil(userID))
	if err != nil {
		t.Fatalf("error getting user: %v", err.Error())
	}

	return &server.MatchmakerPresence{
		UserId:   account.GetUser().Id,
		Username: account.GetUser().Username,
	}
}

func charge(t *testing.T, nk runtime.NakamaModule, userID string, amount float64) {
	err := nk.WalletUpdate(context.Background(), userID, map[string]interface{}{"coins": amount}, nil, true)
	if err != nil {
		t.Fatalf("error updating wallet: %v", err.Error())
	}
}

func join(nk runtime.NakamaModule, db *sql.DB, runtimeLogger runtime.Logger, m Match, player runtime.Presence, state interface{}, ticks int64, params map[string]string) (interface{}, bool) {
	state, acceptUser, _ := m.MatchJoinAttempt(context.Background(), runtimeLogger, db, nk, nil, ticks, state, player, params)

	if acceptUser {
		state = m.MatchJoin(context.Background(), runtimeLogger, db, nk, nil, ticks, state, []runtime.Presence{player})
	}

	return state, acceptUser
}

func TestJoin(t *testing.T) {
	ctrl := gomock.NewController(t)
	nk, runtimeLogger, db := setup(t)
	tx, err := db.Begin()
	if err != nil {
		return
	}
	defer func() {
		ctrl.Finish()
		_ = tx.Rollback()
	}()

	player := authenticate(t, nk, db)
	charge(t, nk, player.GetUserId(), 3000)

	player2 := authenticate(t, nk, db)
	charge(t, nk, player2.GetUserId(), 5)

	dispatcher := mocks.NewMockMatchDispatcher(ctrl)

	m := Match{}
	state, _, _ := m.MatchInit(context.Background(), runtimeLogger, db, nk, Config21)

	ticks := int64(1);
	state, _ = join(nk, db, runtimeLogger, m, player, state, ticks, map[string]string{
		"password": "12345",
	})
	ticks++
	state = m.MatchLoop(context.Background(), runtimeLogger, db, nk, dispatcher, ticks, state, []runtime.MatchData{})

	mState, _ := state.(MatchState)
	if len(mState.GetPlayers(All)) != 1 && len(mState.GetPlayers(Seated)) != 0 {
		t.Fatalf("expected to have a Player %v sitted", player)
	}
	ticks++
	state, accepted := join(nk, db, runtimeLogger, m, player2, state, ticks, map[string]string{
		"password": "1234",
	})
	ticks++
	state = m.MatchLoop(context.Background(), runtimeLogger, db, nk, dispatcher, ticks, state, []runtime.MatchData{})

	if accepted {
		t.Fatalf("expected to have a Player %v not sitted", player)
	}
}

func TestLoopTerminate(t *testing.T) {
	ctrl := gomock.NewController(t)
	nk, runtimeLogger, db := setup(t)
	tx, err := db.Begin()
	if err != nil {
		return
	}
	defer func() {
		ctrl.Finish()
		_ = tx.Rollback()
	}()

	player := authenticate(t, nk, db)
	charge(t, nk, player.GetUserId(), 3000)

	m := Match{}
	var state, _, _ = m.MatchInit(context.Background(), runtimeLogger, db, nk, Config21)

	dispatcher := mocks.NewMockMatchDispatcher(ctrl)
	dispatcher.
		EXPECT().
		BroadcastMessage(gomock.Eq(OpCodeTerminate), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Eq(false)).
		Return(nil).
		Times(1)

	state = m.MatchLoop(context.Background(), runtimeLogger, db, nk, dispatcher, 1, state, []runtime.MatchData{})
	state = m.MatchLoop(context.Background(), runtimeLogger, db, nk, dispatcher, 2, state, []runtime.MatchData{})

	if state == nil {
		t.Fatalf("expected to terminate the match only after 2 min")
	}

	state = m.MatchLoop(context.Background(), runtimeLogger, db, nk, dispatcher, 2*60+1, state, []runtime.MatchData{})

	if state != nil {
		t.Fatalf("expected to terminate the match")
	}
}

func TestLoop2Players(t *testing.T) {
	ctrl := gomock.NewController(t)
	nk, runtimeLogger, db := setup(t)
	tx, err := db.Begin()
	if err != nil {
		return
	}
	defer func() {
		ctrl.Finish()
		_ = tx.Rollback()
	}()

	player1 := authenticate(t, nk, db)
	charge(t, nk, player1.GetUserId(), 3000)

	player2 := authenticate(t, nk, db)
	charge(t, nk, player2.GetUserId(), 4000)

	player3 := authenticate(t, nk, db)
	charge(t, nk, player3.GetUserId(), 5)

	m := Match{}
	var state, _, _ = m.MatchInit(context.Background(), runtimeLogger, db, nk, Config21)

	dispatcher := mocks.NewMockMatchDispatcher(ctrl)
	ticks := int64(1)
	state = m.MatchLoop(context.Background(), runtimeLogger, db, nk, dispatcher, ticks, state, []runtime.MatchData{})
	state, _ = join(nk, db, runtimeLogger, m, player1, state, ticks, map[string]string{
		"password": "12345",
	})

	state = m.MatchLoop(context.Background(), runtimeLogger, db, nk, dispatcher, ticks, state, []runtime.MatchData{})
	ticks++
	state, _ = join(nk, db, runtimeLogger, m, player2, state, ticks, map[string]string{
		"password": "12345",
	})
	ticks++
	state = m.MatchLoop(context.Background(), runtimeLogger, db, nk, dispatcher, ticks, state, []runtime.MatchData{})

	//dispatcher.
	//	EXPECT().
	//	BroadcastMessage(gomock.Eq(OpCodeSit), gomock.Any(), gomock.Any(), gomock.Any(), gomock.Eq(false)).
	//	Return(nil).
	//	Times(1)
	ticks++
	state = m.MatchLoop(context.Background(), runtimeLogger, db, nk, dispatcher, ticks, state, []runtime.MatchData{
		&server.MatchDataMessage{
			Username: player1.GetUsername(),
			UserID: uuid.FromStringOrNil(player1.GetUserId()),
			OpCode: OpCodeSit,
			Data: []byte{1},
		},
	})

}
