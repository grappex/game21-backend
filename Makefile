COMMIT=`git rev-parse HEAD`
BUILD_TIME=`date -u +%FT%T`
BRANCH=`git rev-parse --abbrev-ref HEAD`
BINARY='plugin_code.so'
BUILD_DIR='/root/data/modules'

all: lint build

get-dep:
	go get -u "github.com/heroiclabs/nakama-common@v1.0.0"
	go get -u "github.com/heroiclabs/nakama/v2/server"

lint:
	gofmt -l -s -w . && go vet -tags "prod" -all ./... && golint -set_exit_status=1 ./...

build:
	go build -buildmode=plugin -trimpath -v -tags "prod" -ldflags "-s -w -X main.commitHash=${COMMIT} -X main.branch=${BRANCH} -X main.buildTime=${BUILD_TIME}" -o ./${BINARY}

build-mocks:
	rm -rf mocks
	go get "github.com/golang/mock/gomock"
	go install "github.com/golang/mock/mockgen"
	mockgen -destination=mocks/nakama_common.go -package=mocks github.com/heroiclabs/nakama-common/runtime MatchDispatcher

build-package:
	make build
	cp ${BINARY} ${BUILD_DIR}
	rm -rf ${BINARY}
