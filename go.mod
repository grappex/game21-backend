module plugin_code

go 1.13

require (
	cloud.google.com/go v0.55.0 // indirect
	contrib.go.opencensus.io/exporter/stackdriver v0.13.0 // indirect
	github.com/RoaringBitmap/roaring v0.4.21 // indirect
	github.com/aws/aws-sdk-go v1.29.29 // indirect
	github.com/blevesearch/bleve v0.8.1 // indirect
	github.com/elazarl/go-bindata-assetfs v1.0.0 // indirect
	github.com/etcd-io/bbolt v1.3.4 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/go-yaml/yaml v2.1.0+incompatible // indirect
	github.com/gobuffalo/envy v1.9.0 // indirect
	github.com/gobuffalo/packd v1.0.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang/mock v1.4.3
	github.com/golang/protobuf v1.3.5
	github.com/gorhill/cronexpr v0.0.0-20180427100037-88b0669f7d75 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.14.3 // indirect
	github.com/heroiclabs/nakama v1.0.0 // indirect
	github.com/heroiclabs/nakama-common v1.4.0
	github.com/heroiclabs/nakama/v2 v2.11.0
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/jmespath/go-jmespath v0.3.0 // indirect
	github.com/lib/pq v1.3.0 // indirect
	github.com/prometheus/client_golang v1.5.1 // indirect
	github.com/prometheus/procfs v0.0.11 // indirect
	github.com/rogpeppe/go-internal v1.5.2 // indirect
	github.com/stretchr/testify v1.5.1
	github.com/tinylib/msgp v1.1.2 // indirect
	github.com/yuin/gopher-lua v0.0.0-20191220021717-ab39c6098bdb // indirect
	go.uber.org/zap v1.14.1
	golang.org/x/crypto v0.0.0-20200320181102-891825fb96df // indirect
	golang.org/x/net v0.0.0-20200320220750-118fecf932d8 // indirect
	golang.org/x/sys v0.0.0-20200321134203-328b4cd54aae // indirect
	google.golang.org/genproto v0.0.0-20200319113533-08878b785e9c // indirect
	google.golang.org/grpc v1.28.0
	gopkg.in/resty.v1 v1.12.0 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

replace go.etcd.io/bbolt => go.etcd.io/bbolt v1.3.3

replace github.com/etcd-io/bbolt => github.com/etcd-io/bbolt v1.3.3
